var Webpack = require('webpack');
var path = require('path');

var nodeModulesPath = path.resolve(__dirname, 'node_modules');
var buildPath = path.resolve(__dirname, 'public', 'build');
var mainPath = path.resolve(__dirname, 'src', 'js', 'main.js');

var config = {

    // Makes sure errors in console map to the correct file
    // and line number
    devtool: 'eval',
    entry: getEntrySources([mainPath]),
  output: {

    // We need to give Webpack a path. It does not actually need it,
    // because files are kept in memory in webpack-dev-server, but an
    // error will occur if nothing is specified. We use the buildPath
    // as that points to where the files will eventually be bundled
    // in production
    path: buildPath,
    filename: 'bundle.js',

    // Everything related to Webpack should go through a build path,
    // localhost:3000/build. That makes proxying easier to handle
    publicPath: '/build/'
  },
  module: {

    loaders: [

      // I highly recommend using the babel-loader as it gives you
      // ES6/7 syntax and JSX transpiling out of the box
      {
        test: /\.(js)$/,
        loader: 'babel-loader',
        exclude: [nodeModulesPath],
        query: {
          presets: ['react']
        }
      },

      // JSON
      {
        test: /\.json$/,
        loader: 'json'
      },

      // SASS
      {
        test: /\.scss$/,
        loaders: [
          'style',
          'css',
          'autoprefixer?browsers=last 3 versions',
          'sass?ouputStyle=expanded'
        ]
      }

    ]
  },

  // We have to manually add the Hot Replacement plugin when running
  // from Node
  plugins: [new Webpack.HotModuleReplacementPlugin()]
};

function getEntrySources(sources) {
  if (process.env.NODE_ENV !== 'production') {
    console.log('Using non-production entry sources');
    // For hot style updates
    sources.push('webpack/hot/dev-server');
    // The script refreshing the browser on none hot updates
    sources.push('webpack-dev-server/client?http://localhost:8080');
  };

  return sources;
};

module.exports = config;
