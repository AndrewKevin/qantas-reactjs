var MoviesActionTypes = require('../constants/actionTypes').movies;
var MoviesServices = require('../services/movies');

var MoviesActions = {
  getAllMovies: function() {
    this.dispatch(MoviesActionTypes.GET_ALL_MOVIES);

    var onSuccess = function(payload) {
      this.dispatch(MoviesActionTypes.GET_ALL_MOVIES_SUCCESS, payload);
    }.bind(this);

    var onError = function(error) {
      this.dispatch(MoviesActionTypes.GET_ALL_MOVIES_ERROR, error);
    }.bind(this);

    MoviesServices.getAllMovies({
      success: onSuccess,
      error: onError
    })
  },

  setMoviesFilter: function(filterParam) {
    this.dispatch(MoviesActionTypes.SET_MOVIES_FILTER, filterParam)
  }
};

module.exports = MoviesActions;
