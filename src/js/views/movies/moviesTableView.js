var React = require('react');

var MoviesTableView = React.createClass({
  propTypes: {
    movies: React.PropTypes.arrayOf(React.PropTypes.object).isRequired
  },

  render: function() {
    var rows = this.renderRows();

    return (
      <table className="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Genres</th>
            <th>Rated</th>
            <th>Language</th>
            <th>Rating</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  },

  renderRows: function () {
    var movieRows = [];

    for (var i = this.props.movies.length - 1; i >= 0; i--) {
      var movie = this.props.movies[i]
      var key = i + '_' + movie.title;
      var row = (
        <tr key={key}>
          <td>{movie.title}</td>
          <td>{movie.genres.join(', ')}</td>
          <td>{movie.rated}</td>
          <td>{movie.language.join(', ')}</td>
          <td>{movie.rating}</td>
        </tr>
      );
      movieRows.push(row);
    }

    return movieRows;
  }
})

module.exports = MoviesTableView;