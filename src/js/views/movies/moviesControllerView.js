var React = require('react');
var Fluxxor = require('fluxxor');

var MoviesTableView = require('./moviesTableView');

var MoviesControllerView = React.createClass({
  mixins: [
    Fluxxor.FluxMixin(React),
    Fluxxor.StoreWatchMixin('moviesStore')
  ],

  getStateFromFlux: function () {
    var flux = this.getFlux();
    return flux.store('moviesStore').getState();
  },

  componentDidMount: function () {
    var flux = this.getFlux();
    flux.actions.movies.getAllMovies();
  },

  render: function () {
    var content = null;

    if (this.state.loading) {
      content = (
        <div>Loading</div>
      );
    } else {
      var filteredMovies = this.state.filteredMovies;
      var movieFilter = this.renderMovieFilter();

      content = (
        <div className="movies">
          <h1 className="title title--center">Movies</h1>
          {movieFilter}
          <MoviesTableView movies={filteredMovies} />
        </div>
      );
    };

    return content;
  },

  renderMovieFilter: function () {
    var movieFilterOptions = [];

    for (var i = this.state.movieGenres.length - 1; i >= 0; i--) {
      var genre = this.state.movieGenres[i]
      var option = (
        <option key={genre} value={genre}>{genre}</option>
      );
      movieFilterOptions.push(option);
    }

    return (
      <div className="form-group movies__movieFilter movies__movieFilter--center">
        <label>Filter by genre:</label>
        <select
          ref="movieFilter"
          value={this.state.filterParam}
          onChange={this.handleMovieFilterChange}>
          <option value="" disabled>Filter by genre...</option>
          <option value="">All</option>
          {movieFilterOptions}
        </select>
      </div>
    );
  },

  handleMovieFilterChange: function () {
    var currentMovieFilter = this.refs.movieFilter.value;
    var flux = this.getFlux();
    flux.actions.movies.setMoviesFilter(currentMovieFilter);
  }
});

module.exports = MoviesControllerView;