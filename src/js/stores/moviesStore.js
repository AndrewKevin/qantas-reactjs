var _ = require('lodash');
var Fluxxor = require('fluxxor');
var MoviesActionTypes = require('../constants/actionTypes').movies

var MoviesStore = Fluxxor.createStore({
  initialize: function() {
    // Initial store properties
    this.loading = false;
    this.filterParam = null;
    this.movies = [];
    this.movieGenres = [];

    this.bindActions(
      MoviesActionTypes.GET_ALL_MOVIES, this.onGetAllMovies,
      MoviesActionTypes.GET_ALL_MOVIES_SUCCESS, this.onGetAllMoviesSuccess,
      MoviesActionTypes.GET_ALL_MOVIES_ERROR, this.onGetAllMoviesError,
      MoviesActionTypes.SET_MOVIES_FILTER, this.onSetMoviesFilter
    )
  },

  getState: function() {
    return {
      filteredMovies: this._getFilterMovies(),
      movies: this.movies,
      movieGenres: this.movieGenres
    }
  },

  onGetAllMovies: function() {
    this.loading = true;
    this.emit('change');
  },

  onGetAllMoviesSuccess: function(movies) {
    this.loading = false;
    this.movies = movies;
    this.movieGenres = this._getMovieGenres(movies);
    this.emit('change');
  },

  onGetAllMoviesError: function(error) {
    // TODO
    debugger;
  },

  onSetMoviesFilter: function(filterParam) {
    this.filterParam = filterParam;
    this.emit('change');
  },

  _getFilterMovies: function() {
    var result = null;

    if (this.filterParam) {
      result = _.filter(this.movies, function(movie) {
        return _.indexOf(movie.genres, this.filterParam) > -1;
      }.bind(this));
    } else {
      result = this.movies
    };

    return result;
  },

  _getMovieGenres: function(movies) {
    var genres = [];

    // Grab all genres
    for (var i = movies.length - 1; i >= 0; i--) {
      var movie = movies[i]
      genres = genres.concat(movie.genres);
    };

    // Unique genres
    genres = _.uniq(genres);

    return genres;
  }
});

module.exports = MoviesStore;
