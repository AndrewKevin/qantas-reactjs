require('../stylesheets/master.scss');

var React    = require('react');
var ReactDOM = require("react-dom");
var Fluxxor  = require("fluxxor");

// Fluxxor
var stores  = require('./stores');
var actions = require('./actions');

var flux = new Fluxxor.Flux(stores, actions);
flux.setDispatchInterceptor(function(action, dispatch) {
  ReactDOM.unstable_batchedUpdates(function() {
    dispatch(action);
  });
});


// Render
var renderNode = document.getElementById('app');
var MoviesControllerView = require('./views/movies/moviesControllerView');
var content = (
  <div className="page">
    <MoviesControllerView flux={flux}/>
  </div>
);

ReactDOM.render(content, renderNode);